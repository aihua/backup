#!/bin/bash

USER="user"
PASSWORD="password"
DATABASE="dbname"
SERVER="user@host"
ROUTE="my/server/route/"
DD=$(date +%d)
WW=$(date +%V)
MM=$(date +%m)
YY=$(date +%y)
case $1 in
	0)
		FINAL="/my/route/daily/$DATABASE/$DD-$MM-$YY.sql"
		mysqldump --user=$USER --password=$PASSWORD $DATABASE > $FINAL
		gzip $FINAL
		scp $FINAL".gz" $SERVER:$ROUTE/daily/$DATABASE/
		;;
	1)
		FINAL="/my/route/weekly/$DATABASE/$WW-$YY.sql"
		mysqldump --user=$USER --password=$PASSWORD $DATABASE > $FINAL
		gzip $FINAL
		scp $FINAL".gz" $SERVER:$ROUTE/weekly/$DATABASE/
		;;
	2)
		FINAL="/my/route/monthly/$DATABASE/$MM-$YY.sql"
		mysqldump --user=$USER --password=$PASSWORD $DATABASE > $FINAL
		gzip $FINAL
		scp $FINAL".gz" $SERVER:$ROUTE/monthly/$DATABASE/
		;;
	3)
		FINAL="/my/route/annually/$DATABASE/$YY.sql"
		mysqldump --user=$USER --password=$PASSWORD $DATABASE > $FINAL
		gzip $FINAL
		scp $FINAL".gz" $SERVER:$ROUTE/annually/$DATABASE/
		;;
esac